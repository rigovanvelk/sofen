# Software-Engineering

To start the application execute the start scripts:
```bash
sh start.sh
```

If cannot login execute startscript again:
```bash
sh start.sh

```


Accounts:
    -admin 
        - email : rigo@gmail.com , passw : admin
    -tutee
        - email : fons@gmail.com , passw : admin
     -tutor
        - email : jonas@gmail.com , passw : admin


### Docker commands

Development build:
```bash
sudo docker-compose -f docker-compose.dev.yml up --build
```

Remove containers:
```bash
sudo docker-compose -f docker-compose.yml down
```

Restarting docker service:
```bash
docker-compose up -d --no-deps --build <service-name>
```

Insert data:
```bash
docker exec -it backend scripts/insert_data.sh
```

### Vue project

`localhost:80`

Run linter:

```bash
cd frontend
npm run lint --fix
```

### Django project

`localhost:8000`

Django admin portal: `localhost:80/admin` or `localhost:8000/admin`


View `backend/README.md` for more info
