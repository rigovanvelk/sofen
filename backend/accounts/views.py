from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import SuspiciousOperation
from django.db.models import Q
from mails.views import sendMail
from rest_framework.parsers import JSONParser, FileUploadParser, MultiPartParser
from django.http import HttpResponse, HttpResponseNotFound
from django.contrib.auth import get_user_model
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.permissions import (
    IsAuthenticated,
)
import os.path
import os
from django.http import Http404
from rest_framework import generics
from accounts.avatar import getAvatarFromuser

from .permissions import (
    IsOwnerOrReadOnly,
    IsStaffORIsUser,
    IsStaffORIsOwner,
    IsEmployee,
)

from .models import CustomUser, Notifications
from .serializers import (
    UserSerializer,
    AdvancedUserSerializer,
    UserEditSerializer,
    NotificationSerializer,
    NotificationEditSerializer,
    TutorSerializer,

)

import pathbuilder
import os
import re

from .notify import broadcastAdmins, NotificationType, getlocation
location = getlocation()+"users/"
##
# Users
##
class UsersView(APIView,UpdateModelMixin):
    permission_classes = []

  
    # A regular expression for validating an Email 
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
      
    def isMail(self, email):
    
        if(re.search(regex,email)):  
            return True
            
        else:  
            return False 

    def get_user(self, user_id):
        try:
            return CustomUser.objects.get(pk = str(user_id))
        except CustomUser.DoesNotExist:
            raise Http404

            

    def get(self, request):
        users = CustomUser.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({"users": serializer.data})


    def post(self, request):
        email = request.data.get('email',"")
        first_name = request.data.get('first_name',"")
        last_name = request.data.get('last_name',"")
        password = request.data.get('password',"")
        avatar = request.data.get('avatar')
        contract = request.data.get('contract')
        course = request.data.get('course')
        teaches = request.data.get('teaches',"")
        institution = request.data.get('institution',"")
        is_tutee = request.data.get('is_tutee', False)
        is_tutor = request.data.get('is_tutor', False)

        if (email == '' or first_name == '' or last_name == '' or password == "" or institution == ""):
            return Response('Bad request: required fields are empty', status=400)

        # if (self.isMail(email) == False):
        #     return Response("Email is not valid ", status=412)
        if (is_tutee == False and is_tutor == False):
            return Response('Bad request: User has no function (tutee/tutor)', status=400)
        
        # Check on duplicate email (pk)
        if CustomUser.objects.filter((Q(email=email))).count() > 0:
            #print("Email already in use")
            return Response("Email is already registered ! ", status=409)

        get_user_model().objects.create_user(email, first_name, last_name, password, avatar, contract, course, teaches, institution, is_tutee, is_tutor)
        if is_tutor:
            broadcastAdmins(NotificationType.NT, CustomUser.objects.get(email=email).pk, str(first_name) + " " + str(last_name))
        sendMail(email,"Welcome Mail")
        sendMail(email,"Info werking")
        return Response("successfully created user: " + email)

    
    def patch(self, request, format=None):
        user_id = request.user.id
        
        user = self.get_user(user_id)
        serializer = UserEditSerializer(user, data = request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)


class TutorView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        tutors = CustomUser.objects.filter(is_tutor = True)
        serializer = TutorSerializer(tutors, many=True)
        return Response({"tutors": serializer.data})

class TutorValidatedView(APIView):
     permission_classes = [IsAuthenticated]
     def get(self, request, format=None):
        tutors = CustomUser.objects.filter(is_tutor = True, validated=True)
        serializer = TutorSerializer(tutors, many=True)
        return Response({"tutors": serializer.data})

class TutorValidateView(APIView):
    permission_classes = [IsEmployee]
    def post(self,request, pk):
        if request.user.is_employee == False:
            return Response({"you must be an employee for this action!"}, status=401)
        tutor = CustomUser.objects.get(pk=pk)
        if tutor.is_tutor:
            tutor.validated = "True"
            tutor.save()
            return Response("Tutor validated.")
        else:
            return Response("This user is not a tutor", status=400)


class TutorSearchView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, searchParam, format=None):
        tutors = CustomUser.objects.filter(is_tutor = True)
        serializer = TutorSerializer(tutors, many=True)
        return Response({"tutors": serializer.data })


class UserView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class UserDetailView(generics.RetrieveAPIView):
    permission_classes = [IsStaffORIsUser]
    queryset = CustomUser.objects.all()
    serializer_class = AdvancedUserSerializer


class UserEditView(DestroyModelMixin, UpdateModelMixin, RetrieveAPIView):
    permission_classes = [IsStaffORIsUser]
    queryset = CustomUser.objects.all()
    serializer_class = UserEditSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(self, request, *args, **kwargs)


class UserContractView(APIView):
    permission_classes = [IsStaffORIsOwner]
    """Class to handle everything related to a user contract"""
    parser_classes = (MultiPartParser,)

    def get(self, request, user_id):
        """
        HTTP-Endpoint - GET

        Retrieve the contract of a specific user

        :param user_id: The user id ofthe assignment you want to retrieve the contract from
        :type user_id: integer

        :return: Appropriate http response
        :rtype: binary
        """
        try:
            with open(os.getcwd()+pathbuilder.getContractPath(user_id), 'rb') as f:
                file_data = f.read()
                response = HttpResponse(file_data, content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="Contract.pdf"'
                return response
        except:
            return HttpResponseNotFound()

    def post(self, request, user_id, format=None):
        """
        HTTP-Endpoint - POST

        Endpoint to upload a user contract. Data passed as binary in request body

        :param user_id: id of the user you want to upload a contract for
        :type user_id: integer


        :return: appropriate http response
        """

        # Save the uploaded pdf-file
        try:
            file_obj = request.data['contract.pdf']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "pdf"):
                return Response("Wrong file type: only support pdf", status=415)
        except KeyError:
            return Response(status=500)
       
        os.makedirs(os.getcwd()+pathbuilder.getContractDirectory(user_id), exist_ok=True)

        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(os.getcwd()+pathbuilder.getContractPath(user_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        #Notify admins   
        user_name = CustomUser.objects.get(pk = str(user_id)).get_full_name()
        broadcastAdmins(NotificationType.C, user_id, user_name)

        return Response(status=201)

    def put(self, request, user_id, format=None):
        """
        HTTP-Endpoint - PUT

        Endpoint to update an existing user contract

        :param user_id: id of the user contract that is begin updated
        :type user_id: integer


        :return: appropriate http-response
        """

        print("Updating user contract file for user_id: " + str(user_id))

        # Save the uploaded zip-file
        try:
            file_obj = request.data['contract.pdf']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "pdf"):
                return Response("Wrong file type: only support pdf", status=415)
        except KeyError:
            return Response(status=500)

        # overwrite existing contract file
        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(os.getcwd()+pathbuilder.getContractPath(user_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        return Response(status=200)



class UserAvatarView(APIView):
    permission_classes = [IsStaffORIsOwner]
    """Class to handle everything related to a user """
    parser_classes = (MultiPartParser,)

    def get(self, request, user_id):
        """
        HTTP-Endpoint - GET

        Retrieve the avatar of a specific user

        :param user_id: The user id ofthe assignment you want to retrieve the avatar from
        :type user_id: integer

        :return: Appropriate http response
        :rtype: binary
        """
        try:
            file_data=getAvatarFromuser(user_id)
            if(file_data==0): #If no avatar found
                return HttpResponseNotFound()
            response = HttpResponse(file_data, content_type="image/png")
            response['Content-Disposition'] = 'attachment; filename="Avatar.png"'
            return response
        except:
                return HttpResponseNotFound()

    def post(self, request, user_id, format=None):
        """
        HTTP-Endpoint - POST

        Endpoint to upload a user avatar. Data passed as binary in request body

        :param user_id: id of the user you want to upload a avatar for
        :type user_id: integer


        :return: appropriate http response
        """

        # Save the uploaded file
        try:
            file_obj = request.data['avatar']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "jpeg" and extension != "jpg" and extension != "png"):
                return Response("Wrong file type: only support jpeg, jpeg, png", status=415)
        except KeyError:
            return Response(status=500)
      
        os.makedirs(os.getcwd()+pathbuilder.getAvatarDirectory(user_id), exist_ok=True)

        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(os.getcwd()+pathbuilder.getAvatarPath(user_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        return Response(status=201)

    def put(self, request, user_id, format=None):
        """
        HTTP-Endpoint - PUT

        Endpoint to update an existing user avatar

        :param user_id: id of the user avatar that is begin updated
        :type user_id: integer


        :return: appropriate http-response
        """

        print("Updating user avatar file for user_id: " + str(user_id))

        # Save the uploaded zip-file
        try:
            file_obj = request.data['avatar']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "jpeg" and extension != "jpg" and extension != "png"):
                return Response("Wrong file type: only support jpeg, jpeg, png", status=415)
        except KeyError:
            return Response(status=500)

        # overwrite existing avatar file
        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(pathbuilder.getAvatarPath(user_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        return Response(status=200)


##
# Notifications
##
class NotificationsView(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get(self, request, pk):
        if (not (int(request.user.id) == int(pk))):
            return Response({"you must be the owner to see these notifications!"}, status=401)
        offset = int(request.GET.get('offset', 0))
        limit = int(request.GET.get('limit', 20)) + offset
        notifications = Notifications.objects.filter(user=pk).order_by('-date')[offset:limit]
        serializer = NotificationSerializer(notifications, many=True)
        unreadCount = Notifications.objects.filter(user=pk).filter(seen=False).count()
        if serializer.data.__len__() == 0:
            return Response({"notifications": {}, "unread": unreadCount})
        return Response({"notifications": serializer.data, "unread": unreadCount})


class NotificationsNewerView(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get(self, request, user, notification):
        if (not (int(request.user.id) == int(user))):
            return Response({"you must be the owner to do this action!"}, status=401)
        notifications = Notifications.objects.filter(user=user).filter(seqNum__gt=notification).order_by('-date')
        serializer = NotificationSerializer(notifications, many=True)
        unreadCount = Notifications.objects.filter(user=user).filter(seen=False).count()
        if serializer.data.__len__() == 0:
            return Response({"notifications": {}, "unread": unreadCount})
        return Response({"notifications": serializer.data, "unread": unreadCount})

class NotificationsSpecificView(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get(self, request, user, notification):
        if (not (int(request.user.id) == int(user))):
            return Response({"you must be the owner to do this action!"}, status=401)
        notification = Notifications.objects.filter(user=user, seqNum=notification)
        serializer = NotificationSerializer(notification, many=True)
        if serializer.data.__len__() == 0:
            return Response({})
        return Response(serializer.data[0])


class NotificationsEditView(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def put(self, request, user, notification):
        try:
            if (not (int(request.user.id) == int(user))):
                return Response({"you must be the owner to do this action!"}, status=401)
            notification = Notifications.objects.get(user=user, seqNum=notification)
        except:
            return Response("notification not found", status=404)
        notification.seen = True
        notification.save()
        return Response("successfully updated")

    def delete(self, request, user, notification):
        try:
            if (not (int(request.user.id) == int(user))):
                return Response({"you must be the owner to do this action!"}, status=401)
            notification = Notifications.objects.get(user=user, seqNum=notification)
            notification.delete()
            return Response("successfully deleted")
        except:
            return Response("notification not found", status=404)
