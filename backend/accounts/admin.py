# accounts/admin.py
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model

from .models import CustomUser

class CustomUserAdmin(admin.ModelAdmin):
    exclude = ('username', )
    fields = ('email', 'first_name', 'last_name', 'password')
    model = CustomUser
    list_display = ['email', 'first_name', 'last_name']

admin.site.register(CustomUser, CustomUserAdmin)