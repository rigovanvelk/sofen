from django.contrib.auth.models import BaseUserManager


class CustomUsermanager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, first_name, last_name, password, avatar, contract, course, teaches, institution, is_tutee, is_tutor, isSuperuser = False):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        if is_tutee == True and is_tutor == True:
            raise ValueError("is_tutee and is_tutor can't both be 'True'")
        email = self.normalize_email(email)
        user = self.model(email=email)
        user.set_password(password)
        user.first_name = first_name
        user.last_name = last_name
        user.is_superuser = isSuperuser
        user.is_staff = isSuperuser
        user.username = email
        user.avatar = avatar
        user.contract = contract
        user.course = course
        user.teaches = teaches
        user.institution = institution
        user.is_tutee = is_tutee
        user.is_tutor = is_tutor
        user.save(using=self._db)
        return user

    def create_user(self, email, first_name, last_name, password, avatar, contract, course, teaches, institution, is_tutee, is_tutor):
        return self._create_user(email, first_name, last_name, password, avatar, contract, course, teaches, institution, is_tutee, is_tutor, isSuperuser=False)

    def create_superuser(self, email, first_name, last_name, password):

        return self._create_user(email, first_name, last_name, password, isSuperuser=True)