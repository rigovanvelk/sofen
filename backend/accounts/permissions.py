from rest_framework.permissions import BasePermission

class IsTutor(BasePermission):
    message = "You must be a validated tutor for this action!"
    def has_object_permission(self, request, view, obj):
        return request.user.is_tutor and request.user.validated
class IsStaffORIsOwner(BasePermission):
    message = "You must be an admin or the owner of this object"
    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj.user == request.user


class IsStaffORIsUser(BasePermission):
    message = "You must be this user or an admin."
    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj.email == request.user.email
        

class IsStaff(BasePermission):
    message = "You must be an admin"
    def has_object_permission(self, request, view, obj):
        return request.user.is_staff


class IsUser(BasePermission):
    message = 'You must be this user.'
    def has_object_permission(self, request, view, obj):
        return obj.email == request.user.email


class IsOwnerOrReadOnly(BasePermission):
    message = 'You must be the owner of this object.'

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsEmployee(BasePermission):
    message = "You must be an employee for this action!"
    def has_object_permission(self, request, view = 0, obj = 0):
        return request.user.is_employee


class IsEmployeeOrTutorOrTutee(BasePermission):
    message = "You must be a valid user for this action!"
    def has_object_permission(self, request, view = 0, obj = 0):
        return request.user.is_employee or (request.user.is_tutor and request.user.validated) or request.user.is_tutee

class IsStaffORIsTuteeOfRequest(BasePermission):
    message = "You must be the tutee of the request or an admin."
    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj.tutee_id == request.user