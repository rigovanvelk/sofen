from enum import Enum


class NotificationType(str, Enum):
    UN = "undefined"
    # admin
    NT = "new tutor"
    SU = "status update"
    P = "paid"
    C = "contract"
    # tutor
    R = "request"
    L = "linked"
    # tutee
    IP = "in progress"
    FT = "found tutor"
    GS = "give score"
    RQ = "request cancelled"
    RQP = "request cancelled payback"