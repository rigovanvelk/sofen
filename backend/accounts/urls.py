from django.urls import path, include
from django.conf.urls import url

from .views import (
    UsersView,
    UserView,
    UserDetailView,
    UserEditView,
    NotificationsView,
    NotificationsNewerView,
    NotificationsSpecificView,
    NotificationsEditView,
    TutorView,
    TutorSearchView,
    TutorValidateView,
    UserContractView,
    UserAvatarView,
    TutorValidatedView,
    )
    

urlpatterns = [
    path('', UsersView.as_view(), name='users resource'), #remove after next meeting
    url(r'^(?P<pk>\d+)/$', UserView.as_view(), name='basic user info'),
    url(r'^(?P<pk>\d+)/edit/$', UserEditView.as_view(), name='edit basic user info'),
    url(r'^detail/(?P<pk>\d+)/$', UserDetailView.as_view(), name='detailed user info'),
    url(r'^(?P<pk>\d+)/validate/$', TutorValidateView.as_view(), name='validate tutor'),
    url(r'^(?P<pk>\d+)/notifications/$', NotificationsView.as_view(), name='get latest notifications'),
    url(r'^(?P<user>\d+)/notifications/(?P<notification>\d+)/new/$', NotificationsNewerView.as_view(), name='get newer not than seqnum'),
    url(r'^(?P<user>\d+)/notifications/(?P<notification>\d+)/$', NotificationsSpecificView.as_view(), name='get specific notifications'),
    url(r'^(?P<user>\d+)/notifications/(?P<notification>\d+)/edit/$', NotificationsEditView.as_view(), name='Mark notification as seen'),
    path('', UserView.as_view(), name='create user'),
    path('tutors/', TutorView.as_view(), name='get-tutors'),
    path('tutors/validated/', TutorValidatedView.as_view(), name='get-validated-tutors'),
    path('tutors/<str:searchParams', TutorSearchView.as_view(), name='search-tutors'),
    path('contract/<user_id>/',UserContractView.as_view() , name='user contract'),
    path('avatar/<user_id>/',UserAvatarView.as_view() , name='user contract'),
]