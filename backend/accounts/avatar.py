

import pathbuilder
import os
import re
from rest_framework.response import Response
from django.http import HttpResponse, HttpResponseNotFound
import os.path

#returns file_data of specific user 
#returns default if no avatar found 
#returns 0 if filesystem is broke

def getAvatarFromuser(user_id):
    user_id=str(user_id)    
    try:
      
        with open(os.getcwd()+str(pathbuilder.getAvatarPath(user_id)), 'rb') as f:
            file_data = f.read()
            return file_data
    except: #
            try:
                with open(os.getcwd()+'/filesystem/default/avatar.png', 'rb') as f:
                        file_data = f.read()
                        return file_data
                        
            except:
                return 0