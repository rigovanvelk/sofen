
from rest_framework import serializers


from django.core.files.uploadedfile import SimpleUploadedFile

from django.core.files.base import ContentFile
import sys
import base64
from base64 import b64encode


from .models import CustomUser, Notifications
from .avatar import  getAvatarFromuser
# Create Basic user
class UserCreateSerializer:
    class Meta:
        fields = ['email', 'first_name', 'last_name', 'password']
        model = CustomUser


class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()

    class Meta:
        fields = ['id', 'first_name', 'last_name', 'avatar', 'course']
        model = CustomUser

    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image
     

class AdvancedUserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    class Meta:
        fields = ['email', 'first_name', 'last_name', 'avatar', 'contract',
                  'course','institution', 'teaches', 'average_score',
                  'is_tutee', 'is_tutor', 'is_employee','validated']
        model = CustomUser

    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image
class UserEditSerializer(serializers.ModelSerializer):
    class Meta:
        fields = [ 'first_name', 'last_name',  'contract',
                  'course','institution', 'teaches']
        model = CustomUser

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['seqNum', 'date', 'type', 'resource', 'subject', 'seen']
        model = Notifications
    def create(self, validated_data):
        return CustomUser.objects.create(**validated_data)


class NotificationEditSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['seen']
        model = Notifications


class CustomUserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'avatar', 'course')
        model = CustomUser

    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image

class TutorSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    score = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()
    
    class Meta:
        fields = ['id', 'full_name', 'avatar', 'course', 'score', "teaches", "date_joined", 'validated']
        model = CustomUser

    def get_full_name(self, obj):
        return '{} {}'.format(obj.first_name.capitalize(), obj.last_name.capitalize())

    def get_score(self, obj):
        if(obj.average_score):
            return '{}'.format(int(obj.average_score * 20))
        return obj.average_score
    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image
