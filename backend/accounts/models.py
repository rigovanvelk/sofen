#accounts/models.py
from django.db import models
from django.contrib.auth.models import AbstractUser

from .managers import CustomUsermanager
from .notificationtype import NotificationType

# Create your models here.
class CustomUser(AbstractUser):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(('email address'), unique=True)
    avatar = models.CharField(default='https://cdn.vuetifyjs.com/images/lists/1.jpg', max_length=200, null=True)
  
    contract = models.CharField(default=None, null=True, max_length=200)
    course = models.CharField(default="", max_length=100)
    institution = models.CharField(default="", max_length=100)
    teaches = models.CharField(default=" ", max_length=500, null=True)
    average_score = models.FloatField(default=None, null=True)
    is_tutee = models.BooleanField(default=False)
    is_tutor = models.BooleanField(default=False)
    is_employee = models.BooleanField(default=False)
    objects = CustomUsermanager()
    validated = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    # def __str__(self):
    #     return str(self.first_name) + ' ' + str(self.last_name)


class Notifications(models.Model):
    user = models.ForeignKey(CustomUser, related_name='+', on_delete=models.CASCADE)
    seqNum = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=50,
                            choices=[(tag, tag.value) for tag in NotificationType], default=NotificationType.UN)
    resource = models.CharField(max_length=400, default="")
    subject = models.CharField(max_length=200, default="")
    seen = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'seqNum',)

    def __str__(self):
        return str(self.type) + str(self.subject)
