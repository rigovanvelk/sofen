from .models import Notifications, CustomUser
from .serializers import NotificationSerializer
from .notificationtype import NotificationType
from mails.views import sendMail

def notify(user, type, resource, subject=""):
    # check seqNum of last notification
    query = Notifications.objects.filter(user=user).order_by('-date')
    if query.count() == 0:
        seqNum = 0
    else:
        serializer = NotificationSerializer(query, many=True)
        seqNum = serializer.data[0]['seqNum']

    userObj = CustomUser.objects.get(pk=user)
    b = Notifications(user=userObj, seqNum=seqNum, type=type, resource=resource, subject=subject)
    b.seqNum = seqNum+1
    b.save()

    # send appropriate mail
    if(type == "request cancelled"):
        sendMail(userObj.email, "Annulatie Mail")
    elif(type == "request cancelled payback"):
         sendMail(userObj.email, "Terugbetalings Mail")
    elif(type == "request"):
        sendMail(userObj.email, "Nieuwe aanvraag")
    elif(type == "found tutor"):
        sendMail(userObj.email, "Bijles gever gevonden")
    elif(type == "linked"):
        sendMail(userObj.email, "Bijles is betaald")
    elif(type == "in progress"):
        sendMail(userObj.email, "Bijlesgever wordt gezocht")
    elif(type == "give score"):
        sendMail(userObj.email, "Review bijles vragen")

def broadcastAdmins(type, resource, subject=""):
    admins = CustomUser.objects.filter(is_employee=True)
    for admin in admins:
        notify(admin.pk, type, resource, subject)

def getlocation():
    return "http://178.62.246.133:8080/api/"