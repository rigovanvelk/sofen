#!/bin/bash

echo "Flushing database"
python manage.py flush --no-input
echo "Inserting test data"
python manage.py loaddata accounts/fixtures/users.json
python manage.py loaddata requests/fixtures/requests.json
python manage.py loaddata requests/fixtures/logs.json
python manage.py loaddata chat/fixtures/rooms.json
python manage.py loaddata chat/fixtures/messages.json
python manage.py loaddata mails/fixtures/mails.json