from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from .helpers import *
from .rooms import *
import json

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()


    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )


    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)   

        data = do_message_action(self.room_name, text_data_json)

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, data)


    def chat_message(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))


    def date(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))


    def error(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))


    def success(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))