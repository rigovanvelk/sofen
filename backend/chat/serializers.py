from rest_framework import serializers
from django.db import models
from .models import CustomUser, Message, Room
from rest_framework.fields import CurrentUserDefault
import base64
from base64 import b64encode
from accounts.views import getAvatarFromuser

class SenderSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = ['id', 'full_name', 'avatar', 'is_employee']

    def get_full_name(self, obj):
        return '{} {}'.format(obj.first_name.capitalize(), obj.last_name.capitalize())

    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image

class DynamicFieldsGetRoomSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsGetRoomSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)
                

class GetRoomSerializer(DynamicFieldsGetRoomSerializer):
    user1 = SenderSerializer(source='user1_id', read_only=True)
    user2 = SenderSerializer(source='user2_id', read_only=True)
    last_message = serializers.SerializerMethodField()
    read = serializers.SerializerMethodField()
    proposer = SenderSerializer(source='proposer_id', read_only=True)

    class Meta:
        model = Room
        fields = ['id', 'user1', 'user2', 'request_id','last_message', 'read', 'proposer', 'date', 'accepted']

    def get_last_message(self, obj):
        return get_last_message_from_chat(obj.pk)

    def get_read(self, obj):
        userid = self.context['request'].user.id
        roomquery = Room.objects.get(pk = obj.pk)
        room = PostRoomSerializer(roomquery).data
        if(userid == room['user1_id']):
            return room['read_user1']
        elif(userid == room['user2_id']):
            return room['read_user2']
        

class PostRoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = Room
        fields = ['id', 'user1_id', 'user2_id', 'request_id', 'read_user1', 'read_user2', 'proposer_id', 'date', 'accepted']


class MessageSerializer(serializers.ModelSerializer):
    sender = SenderSerializer(source='sender_id', read_only=True)

    class Meta:
        model = Message
        fields = ['id', 'room_id', 'sender', 'sent_at', 'message', 'message_type']


class PostMessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ['id', 'room_id', 'sender_id', 'sent_at', 'message', 'message_type']


def get_last_messages_from_chat(chatId, amount):
    queryset = Message.objects.filter(room_id = chatId)
    last_messages = queryset.order_by('-sent_at')[:amount]
    serializer = MessageSerializer(last_messages, many=True)
    return serializer.data


def get_last_message_from_chat(chatId):
    message = get_last_messages_from_chat(chatId, 1)
    if message:
        return message[0]