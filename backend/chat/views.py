from .models import Message, Room, CustomUser
from .serializers import GetRoomSerializer, PostRoomSerializer, MessageSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework .response import Response
from django.http import Http404
from django.db.models import Q
from .helpers import update_room
from .rooms import create_room
from rest_framework.permissions import (
    IsAuthenticated,
)
from rest_framework import status


class ChatView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        queryset = Room.objects.filter(Q(user1_id = request.user.id)|Q(user2_id = request.user.id))
        serializer = GetRoomSerializer(
            queryset, many=True, context={'request': request}, fields=('id', 'request_id', 'user1', 'user2', 'last_message', 'read')
            )
        return Response(serializer.data)


    def post(self, request, format=None):
        try:
            room = create_room(request.data['user1_id'], request.data['user2_id'], request.data['request_id'])
            return Response(room)
        except Exception as e:
            return Response(e)         
        

class UnreadView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        unreadCount = Room.objects.filter(
            Q(user1_id = request.user.id, read_user1 = False) | Q(user2_id = request.user.id, read_user2 = False)
            ).count()
        return Response({'count': unreadCount})


class MessagesListView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, room_id, format=None):
        # Get Messages of the room
        queryset = Message.objects.filter(room_id = room_id)
        messageserializer = MessageSerializer(queryset, many=True)

        # Get the Room
        roomquery = Room.objects.get(pk = room_id)
        roomserializer = GetRoomSerializer(roomquery,  fields=('user1', 'user2', 'request_id', 'proposer', 'date', 'accepted'))

        # Set room as read for this user
        update_room(request.user.id, room_id, True)

        # Return room and messages
        return Response({'room': roomserializer.data, 'messages': messageserializer.data})


class RoomView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, puser1_id, puser2_id, format=None):
        try:
            queryset = Room.objects.get(
                Q(user1_id = puser1_id, user2_id = puser2_id) | 
                Q(user1_id = puser2_id, user2_id = puser1_id)
            )
            roomserializer = PostRoomSerializer(queryset)
            return Response(roomserializer.data)
        except:
            return Response('Room does not exit', status=404)

