from django.urls import path, include

from .views import (
    ChatView,
    MessagesListView,
    UnreadView,
    RoomView
)

urlpatterns = [
    path('', ChatView.as_view(), name='chats'),
    path('unread/', UnreadView.as_view(), name='unread'),
    path('messages/<int:room_id>/', MessagesListView.as_view(), name='messages'),
    path('room/user1/<int:puser1_id>/user2/<int:puser2_id>/', RoomView.as_view(), name='room')
]