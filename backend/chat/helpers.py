from django.shortcuts import render, get_object_or_404
from .models import Message, Room, CustomUser
from .serializers import PostMessageSerializer, SenderSerializer, PostRoomSerializer
from datetime import datetime
from .rooms import *
from requests.helpers import set_date_in_request

# Retrieves json data from the database
def get_sender(sender_id):
    queryset = CustomUser.objects.get(pk = sender_id)
    serializer = SenderSerializer(queryset)
    return serializer.data


# Returns the current time and date in string
def get_current_datetime():
    current_time = datetime.now()
    return str(current_time)


# Does the message action
def do_message_action(room_id, text_data_json):
    data = {}

    # If the message is a chat message
    if text_data_json['type'] == 'chat_message':
        data = chat_action(room_id, text_data_json)
    # If the message is a date proposal
    elif text_data_json['type'] == 'date':
        data = date_action(room_id, text_data_json)
    # If the message is a request to reset the date in the room
    elif text_data_json['type'] == 'reset_date':
        data = reset_date_action(room_id, text_data_json)

    return data


# Saves the message to the database
# Returns error message on fail
def chat_action(room_id, text_data_json):
    # Save the chat message
    data = to_chat_message(text_data_json)
    if not post_message_data(room_id, data):
        data = get_generic_error_message()

    return data


# Updates the room with the proposed date
# Returns error message on fail
def date_action(room_id, text_data_json):
    # Update the room with the new date proposal
    data = to_date_proposal(text_data_json)
    if not update_proposed_date_in_room(room_id, data):
        data = get_generic_error_message()
    if data['accepted']:
        try:
            queryset = Room.objects.get(pk=room_id)
            serializer = PostRoomSerializer(queryset)
            set_date_in_request(serializer.data['request_id'], data['date'], data['sender_id'])
        except Exception as e:
            print(e)
            data = get_generic_error_message()

    return data


# Resets the date of the room
# Returns error message on fail
def reset_date_action(room_id, text_data_json):
    # Reset the date in the room
    if not reset_date_in_room(room_id):
        return get_generic_error_message()
    else:
        return get_generic_success_message()


# Returns a chat message dictionary
def to_chat_message(text_data_json):
    return {
        'type': text_data_json['type'],
        'message': text_data_json['message'],
        'sender': get_sender(text_data_json['sender']),
        'sent_at': get_current_datetime(),
    }


# Returns a date proposal dictionary
def to_date_proposal(text_data_json):
    return {
        'type': text_data_json['type'],
        'date': text_data_json['date'],
        'proposer_id': text_data_json['proposer_id'],
        'sender_id': text_data_json['sender_id'],
        'accepted': text_data_json['accepted'],
    }


def get_generic_error_message():
    return {
        'type': 'error',
        'message': 'Could not send message!'
    }

def get_generic_success_message():
    return {
        'type': 'success',
        'message': 'Successfully sent message!'
    }


def post_message_data(room_id, message_data):
    message_model = {
        'room_id': room_id,
        'sender_id': message_data['sender']['id'],
        'sent_at': message_data['sent_at'],
        'message': message_data['message'],
        'message_type': message_data['type'],
    }
    serializer = PostMessageSerializer(data = message_model)
    if serializer.is_valid():
        serializer.save()
        update_room(message_data['sender']['id'], room_id, True)
        update_room(message_data['sender']['id'], room_id, False)
        return True
    else:
        return False

    
