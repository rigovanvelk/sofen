from django.db import models
from accounts.models import CustomUser
from django.utils import timezone
from django.contrib.auth import get_user_model
from .models import CustomUser
from requests.models import Request


class Room(models.Model):
    user1_id = models.ForeignKey(CustomUser, related_name='+', on_delete=models.CASCADE)
    user2_id = models.ForeignKey(CustomUser, related_name='+', on_delete=models.CASCADE)
    request_id = models.ForeignKey(Request, related_name='+', on_delete=models.CASCADE)
    read_user1 = models.BooleanField(default=False)
    read_user2 = models.BooleanField(default=False)
    proposer_id = models.ForeignKey(CustomUser, related_name='+', on_delete=models.CASCADE, null=True, default=None)
    date = models.DateTimeField(default=None, null=True)
    accepted = models.BooleanField(default=None, null=True)


class Message(models.Model):
    room_id = models.ForeignKey(Room, related_name='+', on_delete=models.CASCADE)
    sender_id = models.ForeignKey(CustomUser, related_name='+', on_delete=models.CASCADE)
    sent_at = models.DateTimeField(default=None, null=True)
    message = models.CharField(max_length=1200)
    message_type = models.CharField(default='chat_message', max_length=20)
