from .models import Message, Room, CustomUser
from .serializers import PostMessageSerializer, SenderSerializer, PostRoomSerializer


# Room id must exist
def update_request_in_room(room_id, request_id):
    room = {
        'request_id': request_id
    }
    try:
        queryset = Room.objects.get(pk = room_id)
    except:
        return Exception('Room does not exist')
    else:
        pass
    serializer = PostRoomSerializer(queryset, data = room, partial = True)

    if serializer.is_valid():
        serializer.save()
        return serializer.data
    else:
        raise Exception(serializer.errors)
    return 


# Creates a room for 2 users
def create_room(puser1_id, puser2_id, request_id):
    try:
        # Check if room already exists
        queryset = Room.objects.get(
            Q(user1_id = puser1_id, user2_id = puser2_id) | 
            Q(user1_id = puser2_id, user2_id = puser1_id)
        )
        serializer = PostRoomSerializer(queryset)
        room_id = serializer.data['id']
        reset_date_in_room(room_id)
        update_request_in_room(room_id, request_id)
        return serializer.data
    except Exception as e:
        # Room does not exist, create room
        room = {
            'user1_id': puser1_id,
            'user2_id': puser2_id,
            'request_id': request_id
        }   
        roomserializer = PostRoomSerializer(data = room)

        if roomserializer.is_valid():
            roomserializer.save()
            return roomserializer.data
        else:
            raise Exception(serializer.errors)


def save_room(roomquery, updated_room):
    serializer = PostRoomSerializer(roomquery, data = updated_room, partial=True)
    if(serializer.is_valid()):
        serializer.save()
        return True
    else:
        return False


def reset_date_in_room(room_id):
    # Get room 
    roomquery = Room.objects.get(pk = room_id)
    room = PostRoomSerializer(roomquery).data

    # Reset room
    updated_room = copy_room(room)
    updated_room['proposer_id'] = None
    updated_room['date'] = None
    updated_room['accepted'] = None

    # Save room
    return save_room(roomquery, updated_room)


def update_proposed_date_in_room(room_id, data):
    # Get room 
    roomquery = Room.objects.get(pk = room_id)
    room = PostRoomSerializer(roomquery).data

    # Update room
    updated_room = copy_room(room)
    updated_room['proposer_id'] = data['proposer_id']
    updated_room['date'] = data['date']
    updated_room['accepted'] = data['accepted']

    # Save room
    return save_room(roomquery, updated_room)


def update_room(sender_id, room_id, read):
    # Get room 
    roomquery = Room.objects.get(pk = room_id)
    room = PostRoomSerializer(roomquery).data

    # Update room
    updated_room = copy_room(room)
    if(read):
        updated_room = set_room_as_read(sender_id, updated_room)
    else:
        updated_room = set_room_as_unread(sender_id, updated_room)

    # Save room
    serializer = PostRoomSerializer(roomquery, data = updated_room, partial=True)
    if serializer.is_valid():
        serializer.save()


def set_room_as_unread(sender_id, updated_room):
    if(updated_room['user1_id'] != sender_id):
        updated_room['read_user1'] = False
    elif(updated_room['user2_id'] != sender_id):
        updated_room['read_user2'] = False

    return updated_room


def set_room_as_read(user_id, updated_room):
    if(updated_room['user1_id'] == user_id):
        updated_room['read_user1'] = True
    elif(updated_room['user2_id'] == user_id):
        updated_room['read_user2'] = True

    return updated_room


def copy_room(room):
    return {
        'id': room['id'],
        'user1_id': room['user1_id'],
        'user2_id': room['user2_id'],
        'read_user1': room['read_user1'],
        'read_user2': room['read_user2'],
        'proposer_id': room['proposer_id'],
        'date': room['date'],
        'accepted': room['accepted']
    }