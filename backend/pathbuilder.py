import os
import dotenv

"""
Module used define globals and generate paths to various files and directories.
"""


USER_FOLDER = "/filesystem/contracts/"
CONTRACT_FOLDER_PREFIX="c-"
CONTRACT_NAME = 'Contract.pdf'

REQUESTS_FOLDER = "/filesystem/requests/"
REQUESTS_FOLDER_PREFIX="r-"
REQUESTS_NAME = 'PaymentProof.png'


AVATAR_FOLDER = "/filesystem/avatars/"
AVATAR_FOLDER_PREFIX="a-"
AVATAR_NAME = 'Avatar.png'



def getContractDirectory(user_id):
    return USER_FOLDER + CONTRACT_FOLDER_PREFIX + user_id

def getContractPath(user_id):
    return getContractDirectory(user_id) + '/' + CONTRACT_NAME

def getPaymentProofDirectory(request_id):
    return REQUESTS_FOLDER + REQUESTS_FOLDER_PREFIX + request_id

def getPaymentProofPath(request_id):
    return getPaymentProofDirectory(request_id) + '/' + REQUESTS_NAME



def getAvatarDirectory(user_id):
    return AVATAR_FOLDER + AVATAR_FOLDER_PREFIX + user_id

def getAvatarPath(user_id):
    return getAvatarDirectory(user_id) + '/' + AVATAR_NAME
