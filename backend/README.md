## Django commands

Run these command in a seperate console while docker-compose is running.

`Creating a new app:`

```bash
docker exec -it backend python3 manage.py startapp <app-name>
```

`Run a test on the app:`

```bash
docker exec -it backend python3 manage.py test <app-name>
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InVzZXIiLCJleHAiOjE1NDA2ODg4MjMsImVtYWlsIjoidXNlckBmb28uY29tIn0.9nXmNoF0dX-N5yh33AXX6swT5zDchosNI0-bcsdSUEk
.
----------------------------------------------------------------------
Ran 1 test in 0.173s

OK
Destroying test database for alias 'default'...
```

`After creating a new model for the database:`

1.
```bash
docker exec -it backend python3 manage.py makemigrations
```

2.
```bash
docker exec -it backend python3 manage.py migrate
```

`Creating a superuser:`

```bash
docker exec -it backend python3 manage.py createsuperuser
Username (leave blank to use 'root'): admin
Email address:
Password:
Password (again):
Superuser created successfully.
```
Leave email address blank and choose whatever password you want.

3.
`Ontvang mails verzonder door systeem:`
sudo docker exec -it backend python -m smtpd -n -c DebuggingServer localhost:1025
