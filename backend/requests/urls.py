from django.urls import path
from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns


from .views import (
    NewRequestList, RequestList, GetRequest, paymentProofView, ReviewRequestView, BasicTutorInfoRequestView
)

urlpatterns = [
    path('', RequestList.as_view(), name='requests'),
    path('specific/', BasicTutorInfoRequestView.as_view(), name='get basic info tutor of requests'),
    url(r'^(?P<pk>\d+)/reviews/$', ReviewRequestView.as_view(), name='Update request review'),
    path('new/', NewRequestList.as_view(), name='new-requests'),
    path('reviews/', ReviewRequestView.as_view(), name='get-reviews of a tutor'),    
    path('payment-proof/<request_id>/',paymentProofView.as_view() , name='paymentProof'),
    path('detail/<int:pk>/', GetRequest.as_view(), name='get-request'),
]

urlpatterns = format_suffix_patterns(urlpatterns)