succes = {
    0: 'Nieuw',
    1: 'Toegekend',
    2: 'Bijlesgever gevonden',
    3: 'Bijlesgever geaccepteerd',
    4: 'Betaald',
    5: 'Betaling goedgekeurd',
    6: 'Datum vastgelegd',
    7: 'Verzekerd',
    8: 'Afgelopen',
    9: 'Terug betalen',
    10: 'Geannuleerd',
    11: 'Afgelopen reviewed'
}

error = {
    50: 'Bijlesgever niet geaccepteerd',
    51: 'Niet betaald',
    52: 'Geen datum vastgelegd',
    53: "Bijles niet succesvol afgelopen",
    54: "Foutieve betaling ontvangen",
    55: "Gerapporteerd"
}

warning = {
    100: 'Niet op tijd verzekerd',
    101 : 'Overgedragen aan andere admin',
    102 : 'Moet terugbetaald worden'
}


def get_status_text(status):
    text = ''
    if is_status_success(status):
        text = succes[status]
    elif is_status_error(status):
        text = error[status]
    elif is_status_warning(status):
        text = warning[status]

    return text

def is_status_success(status):
    return status < 50 and status != 54


def is_status_error(status):
    return status >= 50 and status < 100


def is_status_warning(status):
    return status >= 100