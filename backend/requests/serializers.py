from rest_framework import serializers
from .models import Request, CustomUser, LogRequest
from accounts.views import getAvatarFromuser
import base64
from base64 import b64encode

class DynamicFieldsRequestSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsRequestSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class CustomUserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    class Meta:
        fields = ['id','full_name', 'avatar', 'course']
        model = CustomUser

    def get_full_name(self, obj):
        return '{} {}'.format(obj.first_name.capitalize(), obj.last_name.capitalize())

    def get_avatar(self, obj):        
        data = getAvatarFromuser(obj.id)
        image = b64encode(data).decode("utf-8")  
        return image
     
class LogRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = LogRequest
        fields = ['id', 'request_id', 'status', 'text', 'url', 'timestamp']


class RequestSerializer(DynamicFieldsRequestSerializer):
    tutee = CustomUserSerializer(source='tutee_id', read_only=True)
    tutor = CustomUserSerializer(source='tutor_id', read_only=True)
    logs = LogRequestSerializer(many=True, read_only=True)
    
    class Meta:
        fields = [
            'id', 'tutee', 'tutor', 'admin_id', 'date', 'status', 'text', 'subject', 
            'requested_length', 'created_at', 'deadline', 'logs', 'price', 'last_updated', 'paid'
        ]
        model = Request


class PostRequestSerializer(serializers.ModelSerializer):

    class Meta:
        fields = [
            'id', 'tutee_id', 'tutor_id', 'admin_id', 'date', 'status', 'text', 'subject', 
            'requested_length', 'created_at', 'deadline', 'price', 'last_updated', 'paid'
        ]
        model = Request

class ReviewRequestSerializer(DynamicFieldsRequestSerializer):
    #tutor = CustomUserSerializer(source='tutor_id', read_only=True)
    
    class Meta:
        fields = [
            'id', 'tutee_id','tutor_id', 'score', 'review',
        ]
        model = Request

class BasicTutorInfoRequestSerializer(DynamicFieldsRequestSerializer):
    tutor = CustomUserSerializer(source='tutor_id', read_only=True)
    
    
    class Meta:
        fields = [
             'tutor'
        ]
        model = Request