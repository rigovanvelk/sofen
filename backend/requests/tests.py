from django.test import TestCase

# Create your tests here.

from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status

from rest_framework_jwt.settings import api_settings


class TestRequests(TestCase):
    """Requests Tests"""

    def test_get_requests(self):
        """
        Unauthenticated users should not be able to access posts via APIListView
        """
        url = reverse('requests')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_header_for_token_verification(self):
        """
        https://stackoverflow.com/questions/47576635/django-rest-framework-jwt-unit-test
        Tests that users can access posts with JWT tokens
        """

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        user = User.objects.create_user(username='user', email='user@foo.com', password='pass')
        user.is_active = True
        user.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        verify_url = reverse('api-jwt-verify')
        credentials = {
            'token': token
        }

        resp = self.client.post(verify_url, credentials, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)


from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Requests
from .serializers import RequestSerializer
from datetime import datetime

# tests for views

class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_request(id, tutee_id, tutor_id, admin_id, date, status, subject)
        Requests.objects.create(id=id, tutor_id=tutor_id, admin_id=admin_id, date=date, status=status, subject=subject)

    def setUp(self):
        #add test data
        date = datetime.utcnow().strftime("%Y%m%d")
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')
        self.create_request(1, 1, 1, 1, date, 'Q', 'Een paar onderwerpen om eens te kijken of het werkt')


class GetAllRequestsTest(BaseViewTest):

    def test_get_all_songs(self):
        response = self.client.get(
            reverse("requests", kwargs={"version", "v1"})
        )

        expected = Requests.objects.all()
        serialized = RequestSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
