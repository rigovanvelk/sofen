# Create your views here.
from .models import Request, CustomUser
from .serializers import RequestSerializer, LogRequestSerializer, PostRequestSerializer, ReviewRequestSerializer, BasicTutorInfoRequestSerializer
from rest_framework.parsers import JSONParser, FileUploadParser, MultiPartParser
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework .response import Response
from django.http import HttpResponse, HttpResponseNotFound
from rest_framework import status
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.generics import RetrieveAPIView, ListAPIView
from django.http import Http404
from .helpers import *
import json
from mails.views import sendMail
import pathbuilder
import os
import re

#Permission imports
from rest_framework.permissions import (
    IsAuthenticated,
)
from accounts.permissions import (
    IsEmployee, IsEmployeeOrTutorOrTutee, IsStaffORIsOwner,IsStaffORIsTuteeOfRequest
)


class NewRequestList(generics.ListAPIView):
    permission_classes = [IsEmployee]
    queryset = Request.objects.filter(admin_id = None)
    serializer_class = RequestSerializer

def get_request(self, request_id):
  
        try:
            return Request.objects.get(pk = request_id)
        except Request.DoesNotExist:
            raise Http404


class RequestList(APIView):
    permission_classes = [IsEmployeeOrTutorOrTutee]

    def get_request(self, request_id):
  
        try:
            return Request.objects.get(pk = request_id)
        except Request.DoesNotExist:
            raise Http404


    def get(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        response_data = None
        if request.user.is_employee:
            response_data = self.get_employee_requests(request)
        elif request.user.is_tutor:
            response_data = self.get_tutor_requests(request)
        elif request.user.is_tutee:
            response_data = self.get_tutee_requests(request)

        return Response(response_data)


    def get_tutor_requests(self, request):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        queryset = Request.objects.filter(tutor_id = request.user.id)
        serializer = RequestSerializer(queryset, many=True)
        # if serializer.data['status'] < 3:
        #     serializer.data['tutee']['full_name'] = "Anoniem"
        #     serializer.data['tutee']['avatar'] = None
        return serializer.data


    def get_tutee_requests(self, request):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        queryset = Request.objects.filter(tutee_id = request.user.id)
        serializer = RequestSerializer(queryset, many=True)
        Checkdates()
        # if serializer._data['status'] < 5:
        #     serializer._data['tutor'] = None
        
        return serializer.data


    def get_employee_requests(self, request):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
            
        #Only return if status != cancelled or reviewed
        queryset = Request.objects.filter(admin_id = request.user.id).exclude(status__gte = 10) 
        serializer = RequestSerializer(queryset, many=True)
        Checkdates()
        return serializer.data
    

    def patch(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        request_id = request.data['request']['id']

        try:
            queryset = self.get_request(request_id)
        except Exception as e:
            return Response(e)
        else:
            pass

        request_data = to_simple_request(request.data['request'])
        serializer = PostRequestSerializer(queryset, data = request_data, partial=True)

        if serializer.is_valid():
            try:
                # Create a log
                create_new_log(request_data, request.user.id)
                # Send a notification to the appropriate users
                send_notification(request_data)
                # Do an extra action
                do_extra_action(request_data)
            except Exception as e:
                return Response(e)
            else:
                pass

            # Update the request
            serializer.save()            
            return Response(serializer.data)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    
    def post(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        new_request = {
            'tutee_id': request.user.id,
            'subject': request.data['subject'],
            'deadline': request.data['deadline'],
        }
        serializer = PostRequestSerializer(data = new_request)

        if serializer.is_valid():
            # Save the new request
            serializer.save()
            try:
                # Create a log
                create_new_log(serializer.data, request.user.id)
                # Send a notification to the appropriate users
                send_notification(serializer.data)
            except Exception as e:
                return Response(e)
            else:
                pass

            return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)



class GetRequest(generics.RetrieveAPIView):
    permission_classes = [IsEmployeeOrTutorOrTutee]
    queryset = Request.objects.all()
    serializer_class = RequestSerializer


class ReviewRequestView(UpdateModelMixin,RetrieveAPIView):
    permission_classes = [IsStaffORIsTuteeOfRequest]
    queryset = Request.objects.all()
    serializer_class = ReviewRequestSerializer

    def patch (self, request, *args, **kwargs):
        #update request status
        update_request_status(kwargs['pk'],11,"Afgelopen reviewed")
        return self.update(request, *args, **kwargs)


    def get(self, request):
        tutor_id = request.GET.get('tutor_id','')
        all_reviews = Request.objects.filter(tutor_id = tutor_id).exclude(score = None)
        serializer = ReviewRequestSerializer(all_reviews, many=True)
        return Response({"reviews": serializer.data})


class paymentProofView(APIView):
    permission_classes = [IsStaffORIsOwner]
    """Class to handle everything related to a request """
    parser_classes = (MultiPartParser,)

    def get(self, request, request_id):
        obj = get_request(self,request_id)
        if not (request.user.is_staff or obj.tutee_id == request.user):
            return Response({"You must be a valid user for this action!"}, status=401)
        """
        HTTP-Endpoint - GET

        Retrieve the paymentProof of a specific request

        :param request_id: The request id of the assignment you want to retrieve the paymentProof from
        :type request_id: integer

        :return: Appropriate http response
        :rtype: binary
        """
        try:
            with open(os.getcwd()+pathbuilder.getPaymentProofPath(request_id), 'rb') as f:
                file_data = f.read()
                response = HttpResponse(file_data, content_type="image/png")
                response['Content-Disposition'] = 'attachment; filename="PaymentProof.png"'
                return response
        except:
                return HttpResponseNotFound()

    def post(self, request, request_id, format=None):
        """
        HTTP-Endpoint - POST

        Endpoint to upload a request paymentProof. Data passed as binary in request body

        :param request_id: id of the request you want to upload a paymentProof for
        :type request_id: integer


        :return: appropriate http response
        """
        obj = get_request(self,request_id)
        if not (request.user.is_staff or obj.tutee_id == request.user):
            return Response({"You must be a valid user for this action!"}, status=401)
        # Save the uploaded file
        try:
            file_obj = request.data['paymentProof']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "jpeg" and extension != "jpg" and extension != "png"):
                return Response("Wrong file type: only support jpeg, jpeg, png", status=415)
        except KeyError:
            return Response(status=500)
      
        os.makedirs(os.getcwd()+pathbuilder.getPaymentProofDirectory(request_id), exist_ok=True)

        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(os.getcwd()+pathbuilder.getPaymentProofPath(request_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        return Response(status=201)

    def put(self, request, request_id, format=None):
        """
        HTTP-Endpoint - PUT

        Endpoint to update an existing request paymentProof

        :param request_id: id of the request paymentProof that is begin updated
        :type request_id: integer


        :return: appropriate http-response
        """
        obj = get_request(self,request_id)
        if not (request.user.is_staff or obj.tutee_id == request.user):
            return Response({"You must be a valid user for this action!"}, status=401)
        print("Updating request paymentProof file for request_id: " + str(request_id))

        # Save the uploaded zip-file
        try:
            file_obj = request.data['paymentProof']
            # Check file extension
            extension = str(file_obj).split(".")[-1]
            if (extension != "jpeg" and extension != "jpg" and extension != "png"):
                return Response("Wrong file type: only support jpeg, jpeg, png", status=415)
        except KeyError:
            return Response(status=500)

        # overwrite existing paymentProof file
        try:
            uploaded_file_lines = file_obj.readlines()
        except:
            return Response(status=406)

        file_handle = open(pathbuilder.getPaymentProofPath(request_id), 'wb')
        for line in uploaded_file_lines:
            file_handle.write(line)

        file_handle.flush()
        file_handle.close()

        return Response(status=200)


class BasicTutorInfoRequestView(APIView):
    permission_classes = [IsStaffORIsTuteeOfRequest]
    
    def get(self, request):
        request_id = request.GET.get('request_id','')
        obj = get_request(self,request_id)
        if not (request.user.is_staff or obj.tutee_id == request.user):
            return Response({"You must be a valid user for this action!"}, status=401)        
        request = Request.objects.filter(pk = request_id)
        serializer = BasicTutorInfoRequestSerializer(request, many=True)
        return Response(serializer.data) 