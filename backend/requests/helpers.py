from .serializers import LogRequestSerializer, PostRequestSerializer
from .models import Request
from datetime import datetime
from .logstatus import *
from accounts.notificationtype import *
from accounts.notify import *
from mails.views import sendMail
from chat.rooms import create_room
from background_task import background
from datetime import datetime
from requests.serializers import RequestSerializer


#If status == 7 and deadline passed => session has happened
#@background(schedule=2)
def Checkdates():
    today= str(datetime.now())
    all_requests = Request.objects.filter(deadline__lt = today, status = 7)
    serializer = RequestSerializer(all_requests, many=True)
    for req in serializer.data:
        id =req['id']
        update_request_status(id, 8, "Afgelopen")

def get_current_datetime():
    current_time = datetime.now()
    return str(current_time)


def set_date_in_request(request_id, date, sender_id):
    try:
        queryset = Request.objects.get(pk = request_id)
    except Exception as e:
        raise e
    else:
        pass

    updated_data = {
        'date': date,
        'status': 6,
        'text': 'Datum vastgelegd',
        'last_updated': get_current_datetime()
    }
    serializer = PostRequestSerializer(queryset, data = updated_data, partial=True)

    if serializer.is_valid():
        # Update the request
        serializer.save()
        try:
            # Create a log
            create_new_log(serializer.data, sender_id)
            # Send a notification to the appropriate users
            send_notification(serializer.data)
        except Exception as e:
            raise e
        else:
            pass


def create_new_log(request_data, user_id):
    log = {
        'request_id': request_data['id'],
        'status': request_data['status'],
        'text': request_data['text'],
        'timestamp': get_current_datetime(),
        'url': get_log_url(request_data, user_id)
    }
    serializer = LogRequestSerializer(data = log)
    if serializer.is_valid():
        serializer.save()
    else:
        raise Exception(serializer.errors)


def get_log_url(request_data, user_id):
    url = None
    if not is_status_success(request_data['status']):
        url = 'api/users/' + str(user_id) + '/'
    if request_data['status'] == 2 or request_data['status'] == 3:
        url = 'api/users/' + str(request_data['tutor_id']) + '/'

    return url


def send_notification(request_data):
    status = request_data['status']

    # Broadcast to admins when new request arrives
    if status == 0:
        broadcastAdmins("undefined", "", "Er is een nieuwe bijlesaanvraag")
   
    # Notify admin
    if status > 0:           
        notify(request_data['admin_id'], "status update", "", request_data['text'])


    # Notify tutee
    if status == 1:
        notify(request_data['tutee_id'], "in progress", "", "We zijn begonnen met het verwerken van je aanvraag!")
    elif status == 3:
        notify(request_data['tutee_id'], "found tutor", "", "We verwachten een betaling van jou!")
    elif status == 8:
        notify(request_data['tutee_id'], "give score", request_data['id'], "Geef je bijlesgever een review!")
    elif status == 9:
        notify(request_data['tutee_id'], "request cancelled payback", "", "Er is geen bijlesgever voor je bijlesgevonden en je krijgt je geld terug")
    elif status == 10:
        notify(request_data['tutee_id'], "request cancelled", "", "Er is geen bijlesgever voor je bijlesgevonden")

    #Notify tutor
    if status == 2:
        notify(request_data['tutor_id'], "request", "", "Er is een nieuwe aanvraag voor jou!")
    elif status == 5:
        notify(request_data['tutor_id'], "linked", "", "Je bent nu gelinkt met jouw student!")


def send_mail(request_data):
    return


def to_simple_request(request_data):
    request = request_data.copy()
    
    if request["tutor"]:
        request["tutor_id"] = request["tutor"]["id"]
    if request['tutee']:
        request["tutee_id"] = request["tutee"]["id"]
    del request["tutor"]
    del request["tutee"]

    # Update last_updated
    request['last_updated'] = get_current_datetime()
    
    return request


def do_extra_action(request_data):
    # If payment is accepted create a chatroom between tutor and tutee
    if request_data['status'] == 5:
        try:
            create_room(request_data['tutor_id'], request_data['tutee_id'], request_data['id'])
        except Exception as e:
            raise e

# function to update the status of a request 

def update_request_status(request_id,status, text, user_id = -1):
    try:
        queryset = Request.objects.get(pk = request_id)
    except Exception as e:
        raise e
    else:
        pass

    updated_data = {
        'status': status,
        'text': text,
        'last_updated': get_current_datetime()
    }
    serializer = PostRequestSerializer(queryset, data = updated_data, partial=True)

    if serializer.is_valid():
        # Update the request
        serializer.save()
        try:
            # Create a log
            create_new_log(serializer.data, user_id)
            # Send a notification to the appropriate users
            send_notification(serializer.data)
        except Exception as e:
            raise e
        else:
            pass