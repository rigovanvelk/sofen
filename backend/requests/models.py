from django.db import models

# Create your models here.

from accounts.models import CustomUser

class Request(models.Model):
    status = models.IntegerField(default=0)
    text = models.CharField(default="Nieuw", max_length=75)
    tutee_id = models.ForeignKey(CustomUser, related_name='creates', on_delete=models.CASCADE)
    tutor_id = models.ForeignKey(CustomUser, related_name='tutors', on_delete=models.SET_NULL, default=None, null=True)
    admin_id = models.ForeignKey(CustomUser, related_name='manages', on_delete=models.SET_NULL, default=None, null=True)
    date = models.DateTimeField(default=None, null=True)
    requested_length = models.IntegerField(default=2)
    created_at = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField(default=None, null=True)
    subject = models.CharField(max_length=500)
    score = models.FloatField(default=None, null=True)
    review = models.CharField(default=None, null=True, max_length=1200)
    price = models.CharField(default="5000", null=True, max_length=100)
    last_updated = models.DateTimeField(auto_now_add=True)
    paid = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['last_updated']


class LogRequest(models.Model):
    request_id = models.ForeignKey(Request, related_name='logs', on_delete=models.CASCADE)
    status = models.IntegerField(default=0)
    text = models.CharField(default="", max_length=75)
    url = models.CharField(default=None, null=True, max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']


 