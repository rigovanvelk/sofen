from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from .models import Payment, Request



class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ['request_id','paymentId', 'status', 'createdAt', 'expiresAt', 'description', 'amount', 'currency', 'qrcode', 'cancel']
        model = Payment



class PaymentStatusSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ['status']
        model = Payment