from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

from requests.models import Request


class Payment(models.Model):
    # request_id = models.ForeignKey(Request, related_name='has', on_delete=models.SET_NULL, null=True)

    request_id = models.IntegerField(null=False, unique=True)

    paymentId = models.CharField(max_length=400,default='')
    status = models.CharField(max_length=100, default='')
    createdAt = models.CharField(max_length=400,default='')
    expiresAt = models.CharField(max_length=400,default='')
    description = models.CharField(max_length=500,default='')
    amount =  models.CharField(max_length=100,default='')
    currency =  models.CharField(max_length=100,default='')
    qrcode = models.CharField(max_length=300,default='') # link
    cancel = models.CharField(max_length=300,default='') # link

    class Meta:
        ordering = ('pk',)
  