# importing the requests library
from pip._vendor import requests as req
import json
# defining the api-endpoint
API_ENDPOINT_CREATE = "https://api.ext.payconiq.com/v3/payments"

CALLBACK_URL ="http://localhost:8000/api/payments/callback/"
# your API key here
API_KEY = "Bearer 3f8a49ec-2d8a-45d2-b837-37e8586fc870"

#Example response is:{"paymentId":"b5641d74ef70320333596fba","status":"PENDING","createdAt":"2019-12-19T19:44:24.882Z","expiresAt":"2019-12-19T20:04:24.882Z","description":"Test payment 12345","reference":"12345","amount":10,"currency":"EUR","creditor":{"profileId":"5dfb6a673c11320008828298","merchantId":"5dfb6a553c11320008828297","name":"JonasLeuckx","iban":"NL61ABNA3605998615","callbackUrl":"https://dummy.network/api/v1/orders/payconiq"},"_links":{"self":{"href":"https://api.ext.payconiq.com/v3/payments/b5641d74ef70320333596fba"},"deeplink":{"href":"HTTPS://PAYCONIQ.COM/PAY/2/B5641D74EF70320333596FBA"},"qrcode":{"href":"https://portal.payconiq.com/qrcode?c=https%3A%2F%2Fpayconiq.com%2Fpay%2F2%2Fb5641d74ef70320333596fba"},"cancel":{"href":"https://api.ext.payconiq.com/v3/payments/b5641d74ef70320333596fba"}}}

#Param : amount : string ("100" = 100 cent = 1 euro)
#Param : description : string
#Param : reference : string = External payment reference used to reference the Payconiq payment in the calling party's system. => request_id

#return :  HTTP response data as json object // https://developer.payconiq.com/online-payments-dock/#create-payment78

def makePayment(amount, description, reference):
        
    # data to be sent to api
    data = {"amount": amount,
            "currency": "EUR",
            "callbackUrl": CALLBACK_URL,
            "description": description,
            "reference": reference,
            "bulkId": "Bulk-1-200"}

    header = {'Authorization':API_KEY,
    'Content-type': 'application/json',
                    'Cache-Control': 'no-cache'}


    # sending post request and saving response as response object
    r = req.post(url=API_ENDPOINT_CREATE, headers=header, data=json.dumps(data))

    # extracting response text
    pastebin_url = r.text
    response= r.status_code

    Object = json.loads(r.text)
    #print(Object)
    return Object
   

#cancel payment @payconiq
def cancelPayment(link):
        header = {'Authorization':API_KEY,
                'Content-type': 'application/json',
                'Cache-Control': 'no-cache'}
        API_ENDPOINT_CANCEL = link
        
        # sending delete request to payconiq
        r = req.delete(url=link, headers=header)
        
        #Object = json.loads(r.text) // IF uncommented => Cancelfunction will give 404 to frontend (because of delay on response )
        #print(Object)
