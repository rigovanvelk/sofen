from django.urls import path, include
from django.conf.urls import url

from .views import (
    PayView,
    PayCallBackView,
    PayCancelView,
    PayStatusView
)

urlpatterns = [
    path('', PayView.as_view(), name='Do payment'),
    path('callback/', PayCallBackView.as_view(), name='Do payment'),
    path('cancel/', PayCancelView.as_view(), name='cancel payment'),
    path('status/', PayStatusView.as_view(), name='get status payment'),
    url(r'^(?P<request_id>\d+)/$', PayStatusView.as_view(), name='get status payment of request_id'),


]