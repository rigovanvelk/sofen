from .serializers import  PaymentSerializer, PaymentStatusSerializer
from django.shortcuts import render
from rest_framework import generics
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework .response import Response
from rest_framework import status
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from django.db.models import Q
from .models import Payment
from rest_framework.permissions import (
    IsAuthenticated,
)
from accounts.permissions import (
    IsEmployee, IsEmployeeOrTutorOrTutee
)
from datetime import datetime


from .pay import makePayment,cancelPayment


class PayView(APIView):
    permission_classes = [IsEmployeeOrTutorOrTutee]
    def post(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        amount = request.data.get('amount','')
        description = request.data.get('description','')
        reference = request.data.get('request_id','')
        

        paymentExist = True #default

        if(amount == '' or description == '' or reference == ''):
            return Response('amount, decreption, request_id missing', status=404)
        try:
            #Check if there is already a paid bill for this request
            existingPayment = Payment.objects.get(Q(request_id = reference)) #If fails => Except
            if(existingPayment.status == "SUCCEEDED"):
                return Response("Request payment is already paid", status=409)
        
        except:    
            paymentExist = False
            
           
        if(paymentExist==True):
            # If already a payment running/ happened
          
            #already paid
            if(existingPayment.status == 'SUCCES'):
                return Response('paid ! payment already exists', status=409)
            else:
                
                #Still need to pay
                #Check if known payment is still running
                
                d = datetime.utcnow()
                now= (d.isoformat("T") + "Z")
              
                #if payment from db expired
                if(existingPayment.expiresAt<now):
                    #Delete payment from db
                    existingPayment.delete()
                    
                else: #if db payment still valid
                #return payment from db to frontend
                    
                    return Response({'Payment': PaymentSerializer(existingPayment).data}, status=201)

        #IF First Pay request for this assignment OR Expired pay request that isn't paid :
            
        payconiqResponse = makePayment(amount, description, reference)

        data = payconiqResponse
        data['request_id'] = payconiqResponse['reference']
        data['qrcode'] = data['_links']['qrcode']['href']
        data['cancel'] = data['_links']['cancel']['href']
    
        serializer = PaymentSerializer(data=data)         
    
        
        if (serializer.is_valid()):
            # print("Input validated")
            # Save to db
            saved_payment = serializer.save()
            savedPayment = Payment.objects.get(Q(request_id = reference))
            # return saved payment
            return Response({'Payment': PaymentSerializer(savedPayment).data}, status=201)
        return Response('PAYMENT NOT CREATED SUCCESSFULLY', status=404)



   
    def get(self, request):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        all_payements = Payment.objects.all()
        serializer = PaymentSerializer(all_payements, many=True)
        return Response({"payments": serializer.data})

# cancel payment : but can only be done if payment is pending 
class PayCancelView(APIView):
    permission_classes = [IsEmployeeOrTutorOrTutee]
    def post(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        request_id = request.data.get('request_id','')
        
        try:
            existingPayment = Payment.objects.get(Q(request_id = request_id))
            # paid payment can't be deleted !
           
            if(existingPayment.status == "SUCCEEDED"):
                return Response("Payment is already paid, so can't be cancelled/deleted", status=409)
            else:
                cancelLink = existingPayment.cancel
            
                cancelPayment(cancelLink)
                
                #Delete payment from db
                existingPayment.delete()
                
        except:
            return Response("Payment doesnt't exist for this request_id", status=404)
        
        return Response('PAYMENT CANCELLED SUCCESSFULLY', status=status.HTTP_200_OK)

class PayStatusView(APIView):
    permission_classes = [IsEmployeeOrTutorOrTutee]
    def get(self, request, format=None):
        if not IsEmployeeOrTutorOrTutee.has_object_permission(self,request):
            return Response({"You must be a valid user for this action!"}, status=401)
        request_id = request.GET.get('request_id','')
        try:
            existingPayment = Payment.objects.get(Q(request_id = request_id))
        except:
            return Response("Payment doesnt't exist for this request_id", status=404)
       
        return Response({'Status': PaymentStatusSerializer(existingPayment).data})

class PayCallBackView(APIView):
    permission_classes = []
    def post(self, request, format=None):
        request_id = request.data.get('reference','')
        status = request.data.get('status','')
        try:
            existingPayment = Payment.objects.get(Q(request_id = request_id))
            existingPayment.status= status
            existingPayment.save()
        except:
            return Response("Payment doesnt't exist for this request_id", status=404)    
        
        return Response('RECEIVED A CALLBACK with payment status : '+ status, status=200)