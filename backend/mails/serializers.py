from rest_framework import serializers

from mails.models import Mails 



class MailSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ['pk','email_type', 'subject', 'content']
        model = Mails


class MailEditSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['email_type','subject','content']
        model = Mails