from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model




class Mails(models.Model):
    email_type = models.CharField(max_length=100, unique=True,default='')
    subject = models.CharField(max_length=400,default='')
    content= models.CharField(max_length=1200, default='')

    class Meta:
        ordering = ('pk',)
  