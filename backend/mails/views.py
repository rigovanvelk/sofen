from .serializers import  MailSerializer, MailEditSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework .response import Response
from rest_framework import status
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from django.db.models import Q
from .models import Mails
from rest_framework.permissions import (
    IsAuthenticated,
)


from accounts.permissions import (
    IsStaff,
)

from django.core.mail import send_mail

#receiver = rigo@mail.com 
#email_type = an email_type that matches an email type from the db
#Returns True if Mail was succesfully delivered to the mail service
        #False if Mail was not succesfully delivered to the mail service
def sendMail(receiver, email_type):
    queryset = Mails.objects.filter(Q(email_type = email_type))
    serializer = MailSerializer(queryset, many=True)
    try:
        send_mail(serializer.data[0].get('subject'),serializer.data[0].get('content'),'studentdevteach@mail.com',[receiver],fail_silently=False)
    except:
        raise Exception('Email was not delivered succesfully to mailservice')


class MailView(APIView):
    permission_classes = [IsStaff]
    def post(self, request, format=None):
        if not ( request.user.is_staff):
            return Response({"You must be an employee for this action!"}, status=401)
        receiver = request.data.get('receiver')
        email_type = request.data.get('email_type')
        queryset = Mails.objects.filter(Q(email_type = email_type))
        serializer = MailSerializer(queryset, many=True)
        send_mail(serializer.data[0].get('subject'),serializer.data[0].get('content'),'studentdevteach@gmail.com',[receiver],fail_silently=False)
        return Response('EMAIL SEND SUCCESSFULLY', status=status.HTTP_200_OK)


    def get(self, request):
        if not ( request.user.is_staff):
            return Response({"You must be an employee for this action!"}, status=401)
        all_mails = Mails.objects.all()
        serializer = MailSerializer(all_mails, many=True)
        return Response({"mails": serializer.data})


class MailEditView(DestroyModelMixin, UpdateModelMixin, RetrieveAPIView):
    permission_classes = [IsStaff]
    queryset = Mails.objects.all()
    serializer_class = MailEditSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    
    def delete(self, request, *args, **kwargs):
        return self.destroy(self, request, *args, **kwargs)
        

class SpecificMailView(APIView):
    permission_classes = [IsStaff]
    
    def get(self, request):
        if not ( request.user.is_staff):
            return Response({"You must be an employee for this action!"}, status=401)
        email_type = request.data.get('email_type')
        mail = Mails.objects.filter(Q(email_type = email_type))
        serializer = MailSerializer(mail, many=True)
        return Response({"mail": serializer.data})