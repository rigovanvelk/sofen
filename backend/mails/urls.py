from django.urls import path, include
from django.conf.urls import url

from .views import (
    MailView,
    MailEditView,
    SpecificMailView
)

urlpatterns = [
    path('', MailView.as_view(), name='send email'),
    url(r'^(?P<pk>\d+)/edit/$', MailEditView.as_view(), name='edit subject and/or content of mail'),
    path('all/', MailView.as_view(), name='get-all emails'),
    path('specific/', SpecificMailView.as_view(), name='specific mail'),
   

]