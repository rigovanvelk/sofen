from accounts.models import *
from requests.models import *
from django.db.models import Q
from requests.serializers import RequestSerializer, ReviewRequestSerializer
from accounts.serializers import TutorSerializer
from rest_framework import generics
from datetime import datetime

#returns number of users who aren't employees
def getUserCount():
    number = CustomUser.objects.filter().exclude(is_employee=True).count()
    return number

#returns number of users who are tutee
def getTuteeCount():
    number = CustomUser.objects.filter(is_tutee=True).count()
    return number

#returns number of users who are tutee
def getTutorCount():
    number = CustomUser.objects.filter(is_tutor=True).count()
    return number

#returns number of users who are tutee and validated
def getTutorValidatedCount():
    number = CustomUser.objects.filter(is_tutor=True, validated=True).count()
    return number

# returns number of requests made for every course
def getCourseCount(course):
	number = 0
	all_requests = Request.objects.filter()
	serializer = RequestSerializer(all_requests, many=True)
	course = courseAbbreviationToFull(course)
	for req in serializer.data:
		if(req['tutee']['course']== course):
			number+=1
	return number

# returns number of requests done for every month (with status 8 & 11)
def getMonthCount():
	counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	all_requests = Request.objects.filter()
	serializer = RequestSerializer(all_requests, many=True)
	for req in serializer.data:
		today= str(datetime.now())
		if(req['status']==8 or req['status']==11):
			# if in right month and in the current year
			if(req['last_updated'][:4] == today[:4]):
				if(req['last_updated'][5:7] < "13" and req['last_updated'][5:7] > "0"):
					counter[int(req['last_updated'][5:7])]+=1
	return counter

def getChartsData(MonthCount):
    charts = {'charts': [            
        	{
        		"name": "Bijlessen per richting",
        		"values": [
	        		{ "label": "INF", "value": getCourseCount('INF') }, 
	        		{ "label": "HI", "value": getCourseCount('HI') }, 
	        		{ "label": "HW", "value": getCourseCount('HW') }, 
	        		{ "label": "IW", "value": getCourseCount('HW') }, 
	        		{ "label": "TEW", "value": getCourseCount('TEW') }
        		]
        	},
        	{
        		"name": "Bijlessen per maand",
        		"values": [
	        		{ "label": "Jan", "value": MonthCount[1] }, 
	        		{ "label": "Feb", "value": MonthCount[2] }, 
	        		{ "label": "Maa", "value": MonthCount[3] }, 
	        		{ "label": "Apr", "value": MonthCount[4] }, 
	        		{ "label": "Mei", "value": MonthCount[5] }, 
	        		{ "label": "Jun", "value": MonthCount[6] }, 
	        		{ "label": "Jul", "value": MonthCount[7] }, 
	        		{ "label": "Aug", "value": MonthCount[8] }, 
	        		{ "label": "Sep", "value": MonthCount[9] }, 
	        		{ "label": "Okt", "value": MonthCount[10] }, 
	        		{ "label": "Nov", "value": MonthCount[11] }, 
	        		{ "label": "Dec", "value": MonthCount[12] }
        		]
        	},
        ]}
    return charts

def courseAbbreviationToFull(full_course):
	switcher = {
        "INF":"Informatica",
        "HI":"Handelsingenieur",
        "HW": "Humane wetenschappen",
        "IW": "Industriële wetenschappen",
        "TEW": "Toegepaste economische wetenschappen",
       
    }

	return switcher.get(full_course, full_course) #second argument is value if no match

def updateAverageScores():
    
    all_tutors = CustomUser.objects.filter(is_tutor = True)
    serializer = TutorSerializer(all_tutors, many=True)
    tutorData = serializer.data
    for tutor in tutorData:
        RequestCount = 0
        TotalScore = 0
        id =tutor['id']
        requests = Request.objects.filter(tutor_id = id)
        serializer = ReviewRequestSerializer(requests, many=True)
        RequestsData = serializer.data
        for req in RequestsData:
            if(req['score'] != None): #if reviewed
                
                score = req['score']
                TotalScore += score
                RequestCount += 1
        #For every tutor, update average score
        if(RequestCount != 0):
            AVG = TotalScore/RequestCount
            tutor = CustomUser.objects.filter(pk = id).update(average_score=AVG)
