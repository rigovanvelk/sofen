from django.urls import path
from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns


from .views import *

urlpatterns = [
    path('tutors/<str:course>/', TutorStatsView.as_view(), name='stats'),
    path('', AdminStatsView.as_view(), name='stats'),
]