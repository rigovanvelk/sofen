# Create your views here.
from .models import *
from .serializers import *
from rest_framework.parsers import JSONParser, FileUploadParser, MultiPartParser
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework .response import Response
from django.http import HttpResponse, HttpResponseNotFound
from rest_framework import status
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.generics import RetrieveAPIView, ListAPIView
from django.http import Http404
from .count import *

#Permission imports
from rest_framework.permissions import (
    IsAuthenticated,
)
from accounts.permissions import (
    IsEmployee,
)
class TutorStatsView(APIView):
    permission_classes = [IsEmployee]
    def get(self, request, format=None):
        return 

class AdminStatsView(APIView):
    permission_classes = [IsAuthenticated]
    

    def get(self, request, format=None):

        if not IsEmployee.has_object_permission(self,request):
            return Response({"You must be an employee for this action!"}, status=401)
        updateAverageScores()
        stats = {
            'userCount': getUserCount(),
            'tutorsCount': getTutorCount(),
            'validatedTutorsCount': getTutorValidatedCount(),
            'tuteeCount': getTuteeCount(),
            'charts': getChartsData(getMonthCount()).get('charts')
        }


        return Response({"stats": stats})

        