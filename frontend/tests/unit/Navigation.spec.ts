// import Vue from 'vue';
// import Vuetify from 'vuetify';

// import Navigation from 'components/Navigation.vue';

// import { mount, createLocalVue, createWrapper } from '@vue/test-utils';

// const localVue = createLocalVue();

// describe('Navigation.vue', () => {
//   let vuetify;

//   beforeEach(() => {
//     vuetify = new Vuetify();
//   });

//   it('Must logout succesful', () => {
//     const originalError = console.error;
//     console.error = jest.fn();

//     const wrapper = mount(Navigation, { localVue, vuetify });

//     // Correct data
//     localStorage.setItem('authenticated', 'true');
//     localStorage.setItem('token', 'azertyjukjhgtfrdfghjkl');

//     const event = jest.fn();
//     const button = wrapper.find({ ref: 'btn_logout' });

//     button.trigger('click');

//     expect(localStorage.getItem('authenticated')).toBe('false');
//     expect(localStorage.getItem('token')).toBe(null);

//     // Not logged in
//     localStorage.setItem('authenticated', 'false');

//     expect(localStorage.getItem('authenticated')).toBe('false');
//     expect(localStorage.getItem('token')).toBe(null);

//     console.error = originalError;
//   });
// });
