import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/test/:id',
    name: 'test',
    component: () => import('@/views/Test.vue'),
  },
  {
    path: '',
    name: 'home',
    component: () => import('@/views/Home.vue'),
  },
  {
    path: '/nieuweaanvragen/',
    name: 'nieuweaanvragen',
    component: () => import('../views/Requests_View.vue'),
  },
  {
    path: '/berichten/',
    name: 'berichten',
    component: () => import('@/views/Messages.vue'),
  },
  {
    path: '/persoonlijkeaanvragen/',
    name: 'persoonlijkeaanvragen',
    component: () => import('../views/Requests_View.vue'),
  },
  {
    path: '/berichten/:roomid',
    name: 'berichtenid',
    component: () => import('@/views/Messages.vue'),
  },
  {
    path: '/aanmelden/',
    name: 'aanmelden',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/zoeken/:requestid',
    name: 'zoeken',
    component: () => import('../views/Admin_lesgever_zoeken.vue'),
  },
  {
    path: '/registreer',
    name: 'registreren',
    component: () => import('../views/Registration.vue'),
  },
  {
    path: '/email',
    name: 'automatische emails',
    component: () => import('../views/Emails.vue'),
  },
  {
    path: '/betaal/:requestid',
    name: 'betalen van bijles',
    component: () => import('../views/Pay.vue'),
  },
  {
    path: '/statistieken/',
    name: 'statistieken',
    component: () => import('../views/Statistics.vue'),
  },
  {
    path: '/review/:requestid',
    name: 'schrijf een review',
    component: () => import('../views/Create_Review.vue'),
  },
  {
    path: '/contact/',
    name: 'contact',
  },
  {
    path: '/help/',
    name: 'help',
  },
  {
    path: '/instellingen/',
    name: 'instellingen',
  },
  {
    path: '/nieuweaanvraag/',
    name: 'nieuweaanvraag',
    component: () => import('../views/New_Request.vue'),
  },
  {
    path: '/bijlesaanvragen/',
    name: 'bijlesaanvragen',
    component: () => import('../views/Tutor_Requests.vue'),
  },
  {
    path: '/aanvragen/',
    name: 'tuteeaanvragen',
    component: () => import('../views/Tutee_Requests.vue'),
  },
  {
    path: '/bijlesgevers/',
    name: 'bijlesgevers',
    component: () => import('../views/Bijlesgevers.vue'),
  },
  {
    path: '/profiel/:profileid',
    name: 'profielID',
    component: () => import('../views/Profile.vue'),
  },
  {
    path: '/profiel',
    name: 'profiel',
    component: () => import('../views/Profile.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
