import colors from 'vuetify/lib/util/colors';

export default {
  primary: '#EA4440',
  accent: '#FFFFFF',
  error: '#EA4440',
  info: '#6699CC',
  success: '#00A878',
  warning: '#EE8434',
  blank: '#9E9E9E',
};
