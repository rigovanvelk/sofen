import axios from 'axios';
import store from '@/store/index';
import { refreshAccesToken } from './authorisatie';

export function isSuccessState(status: number): boolean {
  return status < 50;
}

export function isErrorState(status: number): boolean {
  return status >= 50 && status < 100;
}

export function isWarningState(status: number): boolean {
  return status >= 100;
}

export function getColor(status: number): string {
  let color: string = '';
  if (isSuccessState(status)) {
    color = 'green lighten-5';
  } else if (isErrorState(status)) {
    color = 'red lighten-5';
  } else if (isWarningState(status)) {
    color = 'yellow lighten-5';
  }
  return color;
}

export function getTextColor(status: number): string {
  let color: string = '';
  if (isSuccessState(status)) {
    color = 'green darken-2';
  } else if (isErrorState(status)) {
    color = 'red darken-2';
  } else if (isWarningState(status)) {
    color = 'yellow darken-3';
  }
  return color;
}


export function getLogDataFromUrl(url: string): Promise<any> {
  return new Promise((resolve) => {
    axios({
      method: 'get',
      url: `http://${window.location.hostname}:8000/${url}`,
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${store.getters['app/getAccesToken']}`,
      },
    })
    /* eslint-disable */
          .then((response) => {
            if (response.status === 200) {
              resolve(response.data);
            }
            if (response.status === 401) {
              refreshAccesToken(getLogDataFromUrl(url));
            }
          });
    });
}
