import axios from 'axios';
import store from '@/store/index';
import router from '@/router/index';

// RefreshToken
export async function refreshAccesToken(callback) {
  // refresh token only if authenticated
  if (store.getters['app/getAuthenticated'] === true) {
    await axios({
      method: 'post',
      url: `http://${window.location.hostname}:8000/api/token/refresh/`,
      headers: {
        'content-type': 'application/json',
      },
      data: { refresh: `${store.getters['app/getRefreshToken']}` },
    }).then((response) => {
      store.commit('app/auth_refresh', response.data.access); // Change store
      if (callback != null) {
        callback();
      }
    }).catch((error) => {
      router.push({ path: '/aanmelden/' });
    });
  }
}

// Login triggers loggin function in vuex store
export async function authenticate(formEmail: string, formPassword: string) {
  try {
    if (formEmail !== '' && formPassword !== '') {
      const response = await axios({
        method: 'post',
        url: `http://${window.location.hostname}:8000/api/token/`,
        headers: {
          'content-type': 'application/json',
        },
        data: {
          email: formEmail,
          password: formPassword,
        },
      });
      if (response.status === 200) {
        const token: any = response.data.access; // accesToken
        const parsedToken: any = JSON.parse(atob(token.split('.')[1]));
        const user = {
          token: response.data.access,
          name: parsedToken.name,
          role: parsedToken.role,
          refreshToken: response.data.refresh,
        };
        setInterval(refreshAccesToken.bind(this), 3000000); // refresh AccesToken every 50min.
        store.commit('app/auth_success', user); // Change store
        return false;
      }
    }
    return true;
  } catch (e) {
    return true;
  }
}

export async function register(formEmail: string,
  formPassword: string,
  formName: string,
  formSurname: string,
  formInstitution: string,
  formCourse: string,
  formTutee: boolean,
  formTutor: boolean,
  formTeaches: string) {
  try {
    if (formEmail !== '' && formEmail !== null
    && formPassword !== '' && formPassword !== null
    && (formTutee === true || (formTutor === true && formTeaches !== '' && formTeaches !== null))
    && formName !== '' && formName !== null
    && formSurname !== '' && formSurname !== null
    && formInstitution !== '' && formInstitution !== null
    && formCourse !== '' && formCourse !== null) {
      const response = await axios({
        method: 'post',
        url: `http://${window.location.hostname}:8000/api/users/`,
        headers: {
          'content-type': 'application/json',
        },
        data: {
          email: formEmail,
          first_name: formName,
          last_name: formSurname,
          password: formPassword,
          course: formCourse,
          institution: formInstitution,
          is_tutee: formTutee,
          is_tutor: formTutor,
          teaches: formTeaches,
        },
      });

      if (response.status === 200) {
        const responseSec = await authenticate(formEmail, formPassword);
        if (!responseSec) {
          setInterval(refreshAccesToken.bind(this), 3000000); // refresh AccesToken every 50min.
          return response.status;
        }

        return response.status;
      }

      return response.status;
    }
  } catch (error) {
    return error.response.status;
  }
  return false;
}

export function logout(): void {
  store.commit('app/logout'); // Do logout mutation in store
}
