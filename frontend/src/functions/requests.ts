import axios from 'axios';
import store from '@/store/index';
import { refreshAccesToken } from './authorisatie';


// Updates the request to the backend
export async function updateRequest(prequest: any): Promise<boolean> {
  return new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: `http://${window.location.hostname}:8000/api/requests/`,
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${store.getters['app/getAccesToken']}`,
      },
      data: {
        request: prequest,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          resolve(true);
        }
        if (response.status === 401) {
          refreshAccesToken(updateRequest(prequest));
        }
      })
      .catch((error) => {
        reject(new Error(error));
      });
  });
}

export async function getRequests(requestUrl: string): Promise<any> {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: requestUrl,
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${store.getters['app/getAccesToken']}`,
      },
    })
    /* eslint-disable */
      .then((response) => {
        if (response.status === 200) {
          resolve(response.data);
        }
        if (response.status === 401) {
          refreshAccesToken(getRequests(requestUrl));
        }
      })
      .catch(error => {
        reject(new Error(error));
      })
    });
}


export async function getRequest(requestId: number): Promise<any> {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `http://${window.location.hostname}:8000/api/requests/detail/${requestId}/`,
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${store.getters['app/getAccesToken']}`,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          resolve(response.data);
        } else {
          reject(new Error("Something went wrong"))
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          refreshAccesToken(getRequest(requestId));
        }
      });
  });
}

export function findIndexOfRequest(request: any, requests: any[]): number {
  for(let i: number = 0; i < requests.length; ++i){
    if(requests[i].id === request.id){
      return i;
    }
  }
  return -1;
}
